let
  # Bugs in styx are fixed in latest master
  latestStyx = self: super: {
    styx = super.styx.overrideAttrs(oldAttrs: {
      src = super.fetchFromGitHub {
        owner  = "styx-static";
        repo   = "styx";
        rev    = "cbfd457d136209ebbedfaa0f80db992613fa7b86";
        sha256 = "0623a4k3sk3pknrn2drx2v3h5wcws157ivf9jnzwkzy4xz4mliik";
      };
    });
  };
in
  # Pinned nixos-21.05 as on 28-07-2021
  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/82151321eeaef290b8345803e0b217a261b7c4e1.tar.gz") {
    overlays = [ latestStyx ];
  }
