{---
title = "About me";
---}

Hello! My name is Raghu Kaippully. I am a software developer living in Bangalore, India.

I am interested in functional programming and I have a lot of experience in building robust software - such as web
applications, SaaS, platform services etc - both at an enterprise level and open source. I have led teams in tech and
management roles.

If you have an interesting opportunity, I would like to hear about it. You can reach me via email at
rkaippully@gmail.com.

You can also find me at:

- [Github](https://github.com/rkaippully)
- [Gitlab](https://gitlab.com/rkaippully)
- [LinkedIn](https://www.linkedin.com/in/raghukaippully)
