{ lib }:
{
  siteUrl = "https://haskell-explained.gitlab.io";

  theme = {
    colorScheme  = "08";
    itemsPerPage = 5;

    lib.highlightjs = {
      enable  = true;
      version = "11.1.0";
      style   = "base16/harmonic16-dark";
      extraLanguages = [ "haskell" ];
    };

    site = {
      title       = "Haskell Explained";
      description = "🐈 a b → 🐈 b c → 🐈 a c";
      copyright   = "<div>&copy; Raghu Kaippully</div><div>All rights reserved.</div>";
    };
  };
}
