/*-----------------------------------------------------------------------------
   Init

   Initialization of Styx, should not be edited
-----------------------------------------------------------------------------*/
{ styx
, extraConf ? {}
}:

rec {

  /* Importing styx library
  */
  styxLib = import styx.lib styx;


/*-----------------------------------------------------------------------------
   Themes setup

-----------------------------------------------------------------------------*/

  /* Importing styx themes from styx
  */
  styx-themes = import styx.themes;

  /* list the themes to load, paths or packages can be used
     items at the end of the list have higher priority
  */
  themes = [
    styx-themes.generic-templates
    styx-themes.hyde
    ./themes/site
  ];

  /* Loading the themes data
  */
  themesData = styxLib.themes.load {
    inherit styxLib themes;
    extraEnv = { inherit data pages; };
    extraConf = [ ./conf.nix extraConf ];
  };

  /* Bringing the themes data to the scope
  */
  inherit (themesData) conf lib files templates env;

/*-----------------------------------------------------------------------------
   Data

   This section declares the data used by the site
-----------------------------------------------------------------------------*/

  data = {
    posts = lib.sortBy "date" "dsc" (lib.loadDir { dir = ./data/posts; inherit env; });
    about = lib.loadFile { file = ./data/pages/about.md; inherit env; };
    menu  = [ pages.about ];
  };


/*-----------------------------------------------------------------------------
   Pages

   This section declares the pages that will be generated
-----------------------------------------------------------------------------*/

  pages = rec {

    index = lib.mkSplit {
      title        = "Home";
      basePath     = "/blog/index";
      itemsPerPage = conf.theme.itemsPerPage;
      template     = templates.index;
      data         = posts.list;
    };

    posts = lib.mkPageList {
      data       = data.posts;
      template   = templates.post.full;

      pageFn = data:
        let
          args = builtins.match "([[:digit:]]{4})-([[:digit:]]{2})-([[:digit:]]{2})-(.+)" data.fileData.basename;
          year = builtins.elemAt args 0;
          month = builtins.elemAt args 1;
          day = builtins.elemAt args 2;
          title = builtins.elemAt args 3;
        in
          { path = "/blog/posts/${year}/${month}/${day}/${title}/index.html"; };
    };

    feed = {
      path     = "/blog/feeds/atom.xml";
      template = templates.feed.atom;
      layout   = lib.id;
      items    = lib.take 10 posts.list;
    };

    about = data.about // {
      path     = "/blog/about.html";
      template = templates.page.full;
    };
  };


/*-----------------------------------------------------------------------------
   Site

-----------------------------------------------------------------------------*/

  /* Converting the pages attribute set to a list
  */
  pageList = lib.pagesToList {
    inherit pages;

    default = {
      layout = templates.layout;
    };
  };

  /* Generating the site
  */
  site = lib.mkSite { inherit files pageList; };

}
