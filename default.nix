let
  pkgs = import ./pkgs.nix;
in
  (pkgs.callPackage (import ./site.nix) {}).site
